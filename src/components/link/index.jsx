import React from "react";

import "./link.css"

const Link = () => {
    return (
        <div className="link">
            <input type="button" value={"AF"} className="link-btn" />
        </div>
    )
}

const LinkHover = ({classTextName}) => {
    return (
        <div className="link-hover">
            <h4>AnimatedFred</h4>
            <p><a href="animated@demo.com" className={classTextName}>animated@demo.com</a></p>
        </div>
    )
}

export { Link, LinkHover }