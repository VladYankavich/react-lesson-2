import React from "react";
import Header from "../header";
import Container from "../container";

import "./main.css"

const Main = () => {
    return (
        <>  
            <Header/>
            <Container/>
        </>
    )
}

export default Main