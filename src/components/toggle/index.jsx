import React from "react";

import "./toggle.css"

const Toggle = ({classCheckboxGroup, classCheckboxName, classLabelName, onClickCallback}) => {
    return(
        <div className={classCheckboxGroup}>
            <input type="checkbox" className={classCheckboxName} />
            <label htmlFor="checkbox" className={classLabelName} onClick={onClickCallback}></label>
        </div>
    )
}

const ToggleHover = ({icon, className, text}) => {
    return (
        <div className={className}>
            <img src={icon} alt="" />
            <p>{text}</p>
        </div>
    )
}

export { Toggle, ToggleHover }