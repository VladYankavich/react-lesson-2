import React from "react";
// import logoSearch from "../../icon/search.svg";

import "./search.css"
 
const Search = ({classSearchName, classSearchTextName}) => {
    
    return(
        <div className={classSearchName}>
            <span className="search-icon" />
            <input type="text" value={"Search"} className={classSearchTextName}/>
        </div>
        
    )
}

// const SearchHover = () => {
//     return(

//     )
// }

export {Search}