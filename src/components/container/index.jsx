import React from "react";
import "./container.css";
import Navigation from "../navigation";

const Container = () => {
    return(
        <>
        <div className="container">
            <Navigation />
        </div>
        </>
    )
}

export default Container