import React, { Component } from "react";
import { Badge, BadgeHover } from "../badge";
import { Link, LinkHover } from "../link";
import { Logout, LogoutHover } from "../logout";
import { Search } from "../search";
import { Toggle, ToggleHover } from "../toggle";
import Chevron from "../chevron";

import logout from "../../icon/logout.svg"
import darkmode from "../../icon/darkmode.svg";
import lightmode from "../../icon/lightmode.svg"
import dashboard from "../../icon/dashboard.svg";
import revenue from "../../icon/revenue.svg";
import notifications from "../../icon/notifications.svg";
import analitics from "../../icon/analitics.svg";
import inventory from "../../icon/inventory.svg";

import "./navigation.css";

export default class Navigation extends Component {
    state = {
        hover: false,        
        clickToggle: false
    };

    onNavigationHover = () => {
        this.setState(({ hover }) => {
            return {
                hover: !hover
            }
        })
    }

    onclickToggle = () => {
        this.setState(({ clickToggle }) => {            
            return {
                clickToggle: !clickToggle
            }
        })
    }

    render() {
        let { hover, clickToggle} = this.state;
        let classNavigationName = "navigation";
        let classSearchName = "search";
        let classSearchTextName = "search-text";
        let classChevronName = "chevron";
        let classCheckboxGroup = "checkbox-group"; 
        let classCheckbox = "checkbox";
        let classToggleName = "checkbox-label";
        let classDarkmodeName = "darkmode";
        let classButtonName = "badges-btn";
        let classTextName = "texts";
        let icons = darkmode;
        let texts = "Darkmode";

        if (hover) {
            classNavigationName += " hover";
            classSearchName += " active";
            classChevronName += " chevron-active";
            classCheckboxGroup += " hover-group";
            classDarkmodeName += " active-hover";
        }

        if (clickToggle) {
            classCheckboxGroup += " grey";
            classToggleName += " movement";            
            classNavigationName += " nav-black";            
            classSearchName += " search-grey";
            classSearchTextName += " search-grey";
            classButtonName += " badges-black";
            classChevronName += " chevron-black";
            classTextName += " texts-black";
            classDarkmodeName += " texts-black";
            icons = lightmode;
            texts = "Light mode";
        }

        return (
            <nav className={classNavigationName} onMouseOver={this.onNavigationHover}>
                <div className="chevron-box">
                    <Chevron className={classChevronName} />
                </div>
                <div className="search-box">
                    <Search classSearchName={classSearchName} classSearchTextName={classSearchTextName}/>
                </div>
                <section className="section_1">
                    <Link />
                    <div className="badges">
                        <Badge icon={dashboard} className="dashboard" classButtonName={classButtonName}/>
                        <Badge icon={revenue} className="revenue" classButtonName={classButtonName}/>
                        <Badge icon={notifications} className="notifications" classButtonName={classButtonName}/>
                        <Badge icon={analitics} className="analitics" classButtonName={classButtonName}/>
                        <Badge icon={inventory} className="inventory" classButtonName={classButtonName}/>
                    </div>
                    <Logout icon={logout} className="logout" />                    
                </section>
                    <Toggle classCheckboxGroup={classCheckboxGroup} 
                    classCheckboxName={classCheckbox} 
                    classLabelName={classToggleName} 
                    onClickCallback={this.onclickToggle}/>
                <section className="section_2">
                    <LinkHover classTextName={classTextName}/>
                    <div className="badges">
                        <BadgeHover className="dashboard" text={"Dashboard"} classTextName={classTextName} />
                        <BadgeHover className="revenue" text={"Revenue"} classTextName={classTextName} />
                        <BadgeHover className="notifications" text={"Notifications"} classTextName={classTextName} />
                        <BadgeHover className="analitics" text={"Analitics"} classTextName={classTextName} />
                        <BadgeHover className="inventory" text={"Inventory"} classTextName={classTextName} />
                    </div>                    
                    <LogoutHover className="logout-hover" text={"Logout"} classTextName={classTextName} />                    
                </section>
                    <ToggleHover icon={icons} className={classDarkmodeName} text={texts} />
            </nav>
        )
    }

}

