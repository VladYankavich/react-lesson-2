import React from "react";

import "./badge.css"

const Badge = ({ icon, className, classButtonName }) => {
    return (
        <div className={className}>
            <button className={classButtonName}><img src={icon} alt=""></img></button>
        </div>
    )
}

const BadgeHover = ({ className, text, classTextName }) => {
    return (
        <div className={className}>
            <a href="#" className={classTextName}>{text}</a>
        </div>
    )
}


export { Badge, BadgeHover };