import React from "react";

export const Logout = ({icon, className}) => {
    return (
        <div className={className}>
            <img src={icon} alt=""></img>
        </div>
    )
}

export const LogoutHover = ({ className, text, classTextName }) => {
    return (
        <div className={className}>
            <a href="#" className={classTextName}>{text}</a>
        </div>
    )
}