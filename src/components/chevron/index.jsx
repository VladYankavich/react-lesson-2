import React from "react";

import "./chevron.css"

const Chevron = ({className}) => {
    return (
        <div className={className}>
            <p><span className="arrow">{">"}</span></p>
        </div>
    )
}

export default Chevron;